% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, '..', 'aperiodic', 'tools');
    addpath(genpath(fullfile(pn.tools, 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'Cookdist'));

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load(fullfile(pn.data, 'e01_pls.mat'), ...
    'stat', 'result', 'lvdat', 'lv_evt_list')

result.perm_result.sprob

indLV = 1;

lvdat = reshape(result.boot_result.compare_u(:,indLV), ...
    size(stat.prob,1), size(stat.prob,2), size(stat.prob,3));
stat.prob = lvdat;
stat.mask = lvdat > 2 | lvdat < -2;

%% invert solution

% stat.mask = stat.mask;
% stat.prob = stat.prob.*-1;
% result.vsc = result.vsc.*-1;
% result.usc = result.usc.*-1;

%% plot multivariate brainscores

% maskNaN = double(stat.mask);
% maskNaN(maskNaN==0) = NaN;

h = figure('units','centimeter','position',[0 0 15 10]);
set(gcf,'renderer','Painters')
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.prob(1,:,:),1)));
plot(stat.time-3,statsPlot, 'k')
xlabel('Time [s from stim onset]'); ylabel('BSR');
title({'Pupil dilation changes'; ['p = ', num2str(round(result.perm_result.sprob(indLV),4))]})
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['e01_pls_traces'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
    
%% plot loadings

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

cBrew(1,:) = 2.*[.3 .1 .1];
cBrew(2,:) = 2.*[.3 .1 .1];

idx_outlier = cell(1); idx_standard = cell(1);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % define outlier as lin. modulation of 2.5*mean Cook's distance
    cooks = Cookdist(dataToPlot);
%     cooks = Cookdist(dataToPlot-...
%         repmat(nanmean(dataToPlot,2),1,size(dataToPlot,2))+...
%         repmat(nanmean(nanmean(dataToPlot,2),1),size(dataToPlot,1),1));
    outliers = cooks>2.5*mean(cooks);
    idx_outlier{indGroup} = find(outliers);
    idx_standard{indGroup} = find(outliers==0);
end

h = figure('units','centimeter','position',[0 0 25 10]);
for indGroup = 1:2
    dataToPlot = uData{indGroup}';
    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = dataToPlot(:,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = dataToPlot(:,i)-...
                nanmean(dataToPlot(:,:),2)+...
                repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
            data_nooutlier{i, j} = data{i, j};
            data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            data_ws_nooutlier{i, j} = data_ws{i, j};
            data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            % sort outliers to back in original data for improved plot overlap
            data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    subplot(1,2,indGroup);
    set(gcf,'renderer','Painters')
        cla;
        cl = cBrew(indGroup,:);
        rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
        h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
        view([90 -90]);
        axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);
    minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
    xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
    ylabel('Target load'); xlabel({'PSD Slope'; '[Individually centered]'})

    % test linear effect
    curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['e01_pls_rcp'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
    
%% save individual brainscores & lin. modulation

LV1.data = cat(1,uData{1}',uData{2}');
X = [1 1; 1 2; 1 3; 1 4]; b=X\LV1.data'; 
LV1.linear = b(2,:);
LV1.IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
'1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
'2252';'2258';'2261'};

save(fullfile(pn.data, 'E1_LV1.mat'), 'LV1')
