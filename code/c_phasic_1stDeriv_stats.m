function c_phasic_1stDeriv_stats(group)

%% Assess condition differences in 1st derivative of pupil diameter: YA

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data    = fullfile(rootpath, 'data');
pn.statsOut = fullfile(rootpath, 'data', 'C_stats');
pn.tools   = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools));
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

%% load results

load(fullfile(pn.data, 'G1_summaryPupil.mat'), 'summaryPupil')
load(fullfile(pn.data, 'G1_pupilDataFT.mat'), 'pupilDataFT')

%% retrieve age labels

idx_group = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))<2000;

%% perform statistical testing (CBPA) with FieldTrip: derivative data (unsmoothed)

% restrict to age group
if strcmp(group, 'ya')
    idx_group = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))<2000;
elseif strcmp(group, 'oa')
    idx_group = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))>2000;
elseif strcmp(group, 'yaoa')
    idx_group = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))>0;
end

pupilGroup = [];
curIDs = find(idx_group);
for indID = 1:numel(curIDs)
    for indCond = 1:4
        pupilGroup{indCond, indID}.dataDeriv = permute(squeeze(nanmean(pupilDataFT{indCond}.dataDeriv(curIDs(indID),:,:),2)),[2,1]);
        pupilGroup{indCond, indID}.dimord = 'chan_time';
        pupilGroup{indCond, indID}.time = pupilDataFT{indCond}.time;
        pupilGroup{indCond, indID}.label{1} = 'pupilDia';
    end
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 0;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'dataDeriv';
cfgStat.neighbours      = []; % no neighbors here 

subj = size(pupilGroup,2);
conds = size(pupilGroup,1);
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat] = ft_timelockstatistics(cfgStat, pupilGroup{1,:}, pupilGroup{2,:}, pupilGroup{3,:}, pupilGroup{4,:});

figure; imagesc(stat.mask)

save(fullfile(pn.statsOut, ['c_parametric_1stderiv_',group,'.mat']), 'cfgStat', 'stat*')
