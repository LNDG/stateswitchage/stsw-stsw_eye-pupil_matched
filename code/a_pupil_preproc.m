function a_pupil_preproc(rootpath, id)

% Analyze pupil diameter in the different conditions, here cut based on
% stimulus presentation period

% For z-normalized version, normalization is done within run across all
% time points to control for interindividual baseline differences without
% biasing condition differences within subject.

% 180117 | written for STSW dynamic (adapted from MD)
% 180131 | paths edited, cleaned up, added blink interpolation
%        | changed automatic artifact detection to 2nd derivative of left
%        eye position
%        | added z-score and demeaned version across TOIs of runs
% 180228 | removed behavioral data, required only during plotting
%        | replaced z-score with z-score NaN version
%        | corrected ID range
%        | create dedicated cfg_eye structure with parameters
% 181119 | combined YA + OA
%        | encode in FieldTrip-like structure

% N = 47 YA, N = 53 (excluding pilots);
% 1223: runs 2 through 4 missing
% 1228: run 4 missing

if ismac % run if function is not pre-compiled
    id = '1223'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.behavior    = fullfile(rootpath, '..', '..', 'stsw_beh_task', 'behavior', 'data');
pn.eyeDataEEG    = fullfile(rootpath, '..', '..', 'stsw_eeg_task', '0_preproc', 'data', 'C_EEG_FT');
pn.eyeDataMrk    = fullfile(rootpath, '..', '..', 'stsw_eeg_task', '0_preproc', 'data', 'B_EEG_ET_ByRun');
pn.data_out    = fullfile(rootpath, 'data', 'A_pupilDiameter_cutByStim');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools));
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'naninterp'));
    
% load behavioral data for condition information etc.
load(fullfile(pn.behavior, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'), 'MergedDataEEG', 'IDs_all')

BehavioralIDs = IDs_all; clear IDs_all;

for indRun = 1:4
    try
        disp([id,'_r',num2str(indRun)]);

        load(fullfile(pn.eyeDataEEG, [id,'_r',num2str(indRun),'_dynamic_eyeEEG_Raw']), 'data_eyeEEG');
        load(fullfile(pn.eyeDataMrk, [id,'_r',num2str(indRun),'_dynamic_mrk_eyeEEG.mat']), 'event', 'urevent');

        cfg_eye.AddBlinkLength = 150; % 150 ms surrounding blinks
        cfg_eye.ArtThreshold = 1000;
        cfg_eye.zvaluecutoff = 3;
        cfg_eye.debug = 0;
        cfg_eye.mark_vert_automatic = 1;

        data_eye = data_eyeEEG; clear data_eyeEEG;

        %% Art correction: marking blinks using the pupil channel:
        % create cell array containing artifacts for each trial
        % config.trl is used to indicate the trial onsets
        for i = 1:length(data_eye.trial)
            A = (1:1:length(data_eye.trial{1,i}));

            % exclude / mark as art trials with a pupil value below a fixed
            % threshold
            if ~cfg_eye.debug
                IndBlink = data_eye.trial{1,i}(4,:)<cfg_eye.ArtThreshold;
            else
                IndBlink = data_eye.trial{1,i}(4,:)<-100;
            end
            A        = A(IndBlink)';
            % add constant to correct timing info
            A        = A+length(data_eye.trial{1,1})*(i-1);

            art{1,i} = A;
        end

        % combine artifact information in one vector
        cat_art = [];
        for i = 1:numel(art)
            cat_art = [cat_art; art{i}];
        end

        %% marking occluded pupil / eye movements using vertical eye channel

        artDetec_iterative = 'no';
        artDetec_iterative_go = 1;

        % compute first derivative of Y-movements
        targetChan = find(strcmp(data_eye.label, 'L-GAZE-Y'));
        data_eye.trial{1}(targetChan,:) = [0, 0, abs(diff(diff(data_eye.trial{1}(targetChan,:))))];

        while artDetec_iterative_go == 1

            cfg                     = [];
            cfg.continuous          = 'no';

            % channel selection, cutoff and padding
            cfg.artfctdef.zvalue.channel    = 'L-GAZE-Y';

            cfg.artfctdef.zvalue.cutoff     = cfg_eye.zvaluecutoff;
            cfg.artfctdef.zvalue.trlpadding = 0;
            cfg.artfctdef.zvalue.fltpadding = 0;
            cfg.artfctdef.zvalue.artfctpeakrange =[- cfg_eye.AddBlinkLength/1000 cfg_eye.AddBlinkLength/1000];
            cfg.artfctdef.zvalue.artfctpeak  = 'yes';

            % make the process interactive
            if cfg_eye.mark_vert_automatic == 0
                cfg.artfctdef.zvalue.interactive = 'yes';
            else
                cfg.artfctdef.zvalue.interactive = 'no';
            end

            try
                [cfg, artifact_vert] = ft_artifact_zvalue(cfg, data_eye);

                if isempty(artifact_vert) || strcmp(artDetec_iterative, 'no')
                    artDetec_iterative_go = 0;
                end

                % perform manual padding around detected art

                artifact_vert(:,1) = artifact_vert(:,1) - cfg_eye.AddBlinkLength;
                artifact_vert(:,2) = artifact_vert(:,2) + cfg_eye.AddBlinkLength;

                warning('Reminder: Art padding set to +/- cfg_eye.AddBlinkLength')

                % sometimes there are no art with the current parameter, use catch to avoid crashs
            catch
                artifact_vert(1,1) = 1; artifact_vert(1,2) = 1;
            end

            % set negative sample values to 1;
            artifact_vert(artifact_vert(:,1)<=cfg_eye.AddBlinkLength,1) = 1;

            %% exclude artifacts

            dataPreproc = data_eye.trial{1}(69,:);
            for indArt = 1:size(artifact_vert,1)
               dataPreproc(:,artifact_vert(indArt,1):artifact_vert(indArt,2)) = NaN;
               dataPreproc(:,dataPreproc==0) = NaN; 
            end

            %% interpolate NaNs (linear interpolation)

            dataPreproc = naninterp(dataPreproc, 'linear');

            data_eye.trial{1}(69,:) = dataPreproc;

            %% exclude NaNs from eye channel for iterative outlier detection 

            targetChan = find(strcmp(data_eye.label, 'L-GAZE-Y'));

            leftEyeChan = data_eye.trial{1}(targetChan,:);
            for indArt = 1:size(artifact_vert,1)
               leftEyeChan(:,artifact_vert(indArt,1):artifact_vert(indArt,2)) = NaN;
            end
            leftEyeChan = naninterp(leftEyeChan, 'linear');
            leftEyeChan(isnan(leftEyeChan)) = 1000;
            data_eye.trial{1}(targetChan,:) = leftEyeChan;

            clear artifact_vert;

        end

        %% cut into stimuli (-3000 + 1000; 1000 Hz sampling rate)

        % S 20 to S24: stimulus onset to stimulus offset

        findMat = strfind({event.type}, 'S 20');
        idx = find(~cellfun(@isempty,findMat));
        triggerMatchingVals_stimOnset= [event(idx).latency]';

        findMat = strfind({event.type}, 'S 24');
        idx = find(~cellfun(@isempty,findMat));
        triggerMatchingVals_stimOffset= [event(idx).latency]';

        cfg_eye.includeSamplesPreStim = 3500;
        cfg_eye.includeSamplesPostStim = 1000;
        cfg_eye.includedSamples = [-cfg_eye.includeSamplesPreStim, 3000+cfg_eye.includeSamplesPostStim];
        cfg_eye.samplingRate = 1000;
        cfg_eye.time = 3+(cfg_eye.includedSamples./cfg_eye.samplingRate);
        cfg_eye.time = cfg_eye.time(1):1/cfg_eye.samplingRate:cfg_eye.time(2);
        cfg_eye.time = cfg_eye.time(1:diff(cfg_eye.includedSamples));

        % cut trials in 
        data_Stim_tmp = [];
        for indTrial = 1:64
            tmp = []; tmp = dataPreproc(1,triggerMatchingVals_stimOnset(indTrial)-...
                cfg_eye.includeSamplesPreStim:triggerMatchingVals_stimOffset(indTrial)+...
                cfg_eye.includeSamplesPostStim);
            data_Stim_tmp(indTrial,:) = tmp(1:diff(cfg_eye.includedSamples));
        end

        %zscor_xnan = @(x) bsxfun(@rdivide, bsxfun(@minus, x, mean(x,'omitnan')), std(x, 'omitnan'));

        %data_Stim_tmp_cat = reshape(data_Stim_tmp',1,[]);
        %data_Stim_tmp_cat_z = zscor_xnan(data_Stim_tmp_cat); % z-score within run across all time points
        %data_Stim_tmp_cat_demean = data_Stim_tmp_cat-nanmean(data_Stim_tmp_cat,2);

        %data_Stim_tmp_z = reshape(data_Stim_tmp_cat_z,[],64)';
        %data_Stim_tmp_demean = reshape(data_Stim_tmp_cat_demean,[],64)';

        pupilData.dataStim((indRun-1)*64+1:indRun*64,:) = data_Stim_tmp; clear data_Stim_tmp;
        %pupilData.dataStim_z((indRun-1)*64+1:indRun*64,:) = data_Stim_tmp_z; clear data_Stim_tmp_z;
        %pupilData.dataStim_demean((indRun-1)*64+1:indRun*64,:) = data_Stim_tmp_demean; clear data_Stim_tmp_demean;

    catch
        disp(['Error for ', id,'_r',num2str(indRun)]);
    end
    % add trial infos 
    tmp_ID = ismember(BehavioralIDs,id);
    TrlInfo.StateOrders = MergedDataEEG.StateOrders(:,tmp_ID);
    TrlInfo.Atts = MergedDataEEG.Atts(:,tmp_ID);
    TrlInfo.RTs = MergedDataEEG.RTs(:,tmp_ID);
    TrlInfo.Accs = MergedDataEEG.Accs(:,tmp_ID);
end

% add some info to structure
pupilData.cfg = cfg_eye;
pupilData.dimord = 'subj_rpt_time';
pupilData.time = cfg_eye.time;
pupilData.label = 'pupil';

save(fullfile(pn.data_out,[id, '_pupilData.mat']), 'pupilData', 'TrlInfo');
