clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

c_phasic_1stDeriv_stats('ya')
c_phasic_1stDeriv_stats('oa')
c_phasic_1stDeriv_stats('yaoa')

disp('Step finished!')

close all; clc;