%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolates source and creates 3D and 4D NIFTI files.        %
% Note: Source is interpolated to anatomy files that have been  %
%       resliced to the same dimensions as the source images.   %
% Last modified: Jan. 15, 2014                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_MakeInterpSourceNii_Wrap(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
FTcfg        = Builder.FTcfg;
PipeSettings = Builder.PipeSettings;


% Clear existing Errorlog & Diary:
if exist('ErrorLog_MakeInterpSourceNii.txt', 'file')
    delete('ErrorLog_MakeInterpSourceNii.txt');
end
if exist('Diary_MakeInterpSourceNii.txt', 'file')
    delete('Diary_MakeInterpSourceNii.txt');
end

diary Diary_MakeInterpSourceNii.txt
ErrLog = fopen('ErrorLog_MakeInterpSourceNii.txt', 'a');



%=======================================================%
% GENERATE NIFTI3D AND NIFTI4D FILES FOR INTERP SOURCE: %
%=======================================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['GENERATING RAW-SOURCE NIFTI FILES:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
    for s = 1:length(name.SubjID{g})
        
        MRIdata = LoadFTmat(paths.MRIdata{g}{s}, 'MakeInterpSourceNii');
        if isempty(MRIdata)
            continue;
        end
        
        
        %--- If specified, reslice MRI to source-images: ---%
        %---------------------------------------------------%
        
        if strcmp(PipeSettings.NormSPM.ResliceMri2Src, 'yes')
            
            SourceSample = LoadFTmat(paths.Source{g}{s,1,1}, 'MakeInterpSourceNii');
            if isempty(SourceSample)
                continue;
            else
                SourceBB     = SourceSample.cfg.grid;
                SourceSample = [];  % Free memory
            end
            
            MRIdata = ft_checkdata(MRIdata, 'datatype', 'volume', 'feedback', 'yes');
            MRIdata = ft_convert_units(MRIdata, 'mm');
            
            cfgReslice            = [];
            cfgReslice.resolution = SourceBB.cfg.grid.resolution;  % Get res and BB in mm.
            cfgReslice.xrange     = [SourceBB.xgrid(1), SourceBB.xgrid(end)];
            cfgReslice.yrange     = [SourceBB.ygrid(1), SourceBB.ygrid(end)];
            cfgReslice.zrange     = [SourceBB.zgrid(1), SourceBB.zgrid(end)];
            
            disp('Reslicing MRI to source:')
            MRIdata = ft_volumereslice(cfgReslice, MRIdata)
            
            SourceBB = [];  % Free memory
            
        end
        
        
        
        %==========================%
        % GENERATE 3D NIFTI FILES: %
        %==========================%
        
        for c = 1:length(name.CondID)
            parfor t = 1:size(time.Windows, 1)
                
                SourceData = LoadFTmat(paths.Source{g}{s,c,t}, 'MakeInterpSourceNii');
                if isempty(SourceData)
                    continue;
                end
                
                % Interpolate source to MRI:
                FTcfgInterpSrc = [];
                FTcfgInterpSrc = FTcfg.InterpSrc;
                
                if isempty(FTcfgInterpSrc.parameter)
                    InterpFields = parameterselection('all', SourceData);  % Get volume fields to interpolate.
                    InterpFields(strcmp(InterpFields, 'pos')) = [];        % Don't interpolate 'pos' field.
                    FTcfgInterpSrc.parameter = InterpFields;
                end
                
                disp('Interpolating source image to MRI:')
                Interp = ft_sourceinterpolate(FTcfgInterpSrc, SourceData, MRIdata)
                
                SourceData = [];  % Free memory
                
                % Do approximate conversion to RAS orientation for viewing:
                Interp = ft_convert_coordsys(Interp, 'spm');
                
                % Write 3D NIFTI:
                cfgVolumeWrite = [];
                cfgVolumeWrite = FTcfg.WriteSrc;
                
                CheckSavePath(paths.SourceNii{g}{s,c,t}, 'MakeInterpSourceNii');
                
                MEGpipeline_MakeNifti3D_Guts...
                    (cfgVolumeWrite, Interp, paths.SourceNii{g}{s,c,t})
                
                Interp = [];  % Free memory
                
            end  % Time
        end  % Cond
        
        MRIdata = [];  % Free memory
        
        
        
        %==========================%
        % GENERATE 4D NIFTI FILES: %
        %==========================%
        
        for c = 1:length(name.CondID)
            
            % Make list of files to merge:
            MergeFileList = [];
            MissingFiles  = 0;
            
            for t = 1:size(time.Windows, 1)  % DO NOT PARFOR HERE, want proper order
                MergeFileList{t}  = paths.SourceNii{g}{s,c,t};  %** Is the ",1" needed?
                
                if ~exist(paths.SourceNii{g}{s,c,t}, 'file')
                    [Folder, File, ~] = fileparts(paths.SourceNii{g}{s,c,t});
                    
                    if MissingFiles == 0
                        MissingFiles = 1;
                        fprintf(ErrLog, ['\nERROR: Source 3D .nii file(s) missing:'...
                            '\n %s \n'], Folder);
                    end
                    
                    fprintf(ErrLog, ' - Time: %s \n', [File,'.nii']);
                end
            end
            
            if MissingFiles == 1
                disp('ERROR: Missing 3D .nii file(s). Skipping dataset.')
                continue;
            end
            
            % Merge 3D NIFTI files into 4D NIFTI:
            CheckSavePath(paths.Nifti4DSource{g}{s,c}, 'MakeInterpSourceNii');
            MEGpipeline_MakeNifti4D_Guts(MergeFileList, paths.Nifti4DSource{g}{s,c})
            
            % Remove intermediate 3D NIFTI files:
            if strcmp(PipeSettings.Afni4D.KeepNifti3D, 'no')
                for f = 1:length(MergeFileList)
                    delete([MergeFileList{f}]);
                end
            end
            
        end  % Cond
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group

if strcmp(PipeSettings.Afni4D.KeepNifti3D, 'no')
    if exist([paths.AnalysisID,'/SOURCE_RASorient_Nifti3D'], 'dir')
        system(['rm -R ',paths.AnalysisID,'/SOURCE_RASorient_Nifti3D']);
    else
        disp(['WARNING: '...
            'Could not find SOURCE_RASorient_Nifti3D folder for removal.']);
    end
end



%==========================================================%
% GENERATES TIME-LEGEND FOR INDICES IN NIFTI & AFNI FILES: %
%==========================================================%

if exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
	delete([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end
TimeLegend = fopen('NiftiAfni4D_TimeLegend.txt', 'a');

fprintf(TimeLegend, ['/// IMPORTANT: ///'...
	'\n - In the AFNI-VIEWER, voxel and time indices start at 0.    '...
    '\n   In the data however, voxel and time indices start at 1. \n'...
    '\n - Therefore, when selecting indices for use outside of the  '...
    '\n   AFNI-viewer or AFNI functions, make sure you are using the'...
    '\n   data index, and NOT the AFNI-viewer index. \n\n']);

fprintf(TimeLegend, ['/// TIME LEGEND: ///'...
	'\n AFNI-Viewer ------- AFNI/NIFTI ------- TIME-Interval:'...
	'\n ViewerIndex ------- Data Index ------- In seconds:']);

for t = 1:size(time.Windows, 1)
	fprintf(TimeLegend, '\n---- %s --------------- %s ------------ %s',...
		num2str(t-1), num2str(t), time.str.Windows{t,3});
end

fclose(TimeLegend);
if ~isequal(pwd, paths.AnalysisID)
    movefile('NiftiAfni4D_TimeLegend.txt', [paths.AnalysisID,'/'], 'f');
end



%==========================%
% CHECKS FOR OUTPUT FILES: %
%==========================%

for g = 1:length(name.GroupID)
	for s = 1:length(name.SubjID{g})
		for c = 1:length(name.CondID)
			
			if ~exist(paths.Nifti4DSource{g}{s,c}, 'file')
				fprintf(ErrLog, ['ERROR: Output 4D-NIFTI file missing:'...
					'\n %s \n\n'], paths.Nifti4DSource{g}{s,c});
			end

		end  % Cond
	end  % Subj
end  % Group

if ~exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
	fprintf(ErrLog, ['ERROR: Time-legend file for NIFTI/AFNI files missing:'...
		'\n %s \n\n'], [paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end



%=================%

if exist([pwd,'/ErrorLog_MakeInterpSourceNii.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeInterpSourceNii.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeInterpSourceNii.txt');
    else
        delete('ErrorLog_MakeInterpSourceNii.txt');
    end
end

if exist([pwd,'/ErrorLog_MakeNifti3D.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNifti3D.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNifti3D.txt');
    else
        delete('ErrorLog_MakeNifti3D.txt');
    end
end

if exist([pwd,'/ErrorLog_MakeNifti4D.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNifti4D.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNifti4D.txt');
    else
        delete('ErrorLog_MakeNifti4D.txt');
    end
end

fclose(ErrLog);
diary off
