%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generates latent variable plots for PLS results: %
% Last modified: Jan. 15, 2014                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  MEGpipeline_PlotPLSresLV(PLSresult, PLSrun, Name, OutFolder, OutName)
%
% Inputs:
%  PLSresult        = Output PLSresult structure from nk pls analysis function.
%  PLSrun.Method    = PLS method type (Ex: MCENT, NROT, BHV, etc).
%  PLSrun.SplitHalf = 'yes' or 'no'
%
%  Name.GroupID  = GroupID names for each group in plot (for labels).
%  Name.CondID   = CondID names for each cond in plot (for labels).
%  Name.BhvNames = Behavior names for each bhv in plot (for labels).
%
%  OutFolder = Output /path/folder of PLS plot.
%  OutName   = Output name of PLS plot.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_PlotPLSresLV(PLSresult, PLSrun, name, OutFolder, OutName)

if nargin ~= 5
    error('ERROR: Incorrect number of input arguments. See help for usage info.')
end

PlotErrLog = fopen(['ErrorLog_PlotPLSresLV.txt'], 'a');


% Get general params from PLSresult:
NumGroup = PLSresult.num_groups;
NumCond  = PLSresult.num_conditions;

if ismember(PLSrun.Method, {'BHV', 'MBLOCK', 'NRBHV', 'NRMBLOCK'})
    NumBhv = size(PLSresult.datamatcorrs_lst{1},1) / NumCond;
end


% Check input name structure:
if length(name.GroupID) ~= NumGroup
    fprintf(PlotErrLog, ['ERROR: Incorrect # of groups in name.GroupID'...
        '\n Correct # groups in name.GroupID should be: %s \n\n'], num2str(NumGroup));
    disp('ERROR: Incorrect # groups in name.GroupID. See ErrorLog.')
    return;
end

if length(name.CondID) ~= NumCond
    fprintf(PlotErrLog, ['ERROR: Incorrect # of conds in name.CondID'...
        '\n Correct # conds in name.CondID should be: %s \n\n'], num2str(NumCond));
    disp('ERROR: Incorrect # conds in name.CondID. See ErrorLog.')
    return;
end

if ismember(PLSrun.Method, {'BHV', 'MBLOCK', 'NRBHV', 'NRMBLOCK'})
    if length(name.BhvNames) ~= NumBhv
        fprintf(PlotErrLog, ['ERROR: Incorrect # of bhvs in name.BhvNames'...
            '\n Correct # bhvs in name.BhvNames should be: %s \n\n'], num2str(NumBhv));
        disp('ERROR: Incorrect # bhvs in name.BhvNames. See ErrorLog.')
        return;
    end
end


% Check for potentially significant LV's:
% Note: If SigLV threshold changed, also change in RunPLS & save fcns.
if strcmp(PLSrun.SplitHalf, 'no')
    SigLVs = find(PLSresult.perm_result.sprob < 0.1);
    SigLVs = SigLVs';  % Change to horizontal so for LV loops properly.
    
else
    SigLVs = [];
    NumLVs = size(PLSresult.u, 2);
    
    for LV = 1:NumLVs
        if (PLSresult.perm_splithalf.ucorr_prob(LV) < 0.1 ||...
                PLSresult.perm_splithalf.vcorr_prob(LV) < 0.1 ||...
                PLSresult.perm_result.sprob(LV) < 0.1)
            SigLVs = [SigLVs, LV];
        end
    end
end

if isempty(SigLVs)
    return;
end



%======================================%
% PLOT FIGURE FOR EACH SIGNIFICANT LV: %
%======================================%

for LV = SigLVs
    
    pValue = PLSresult.perm_result.sprob(LV);
    if strcmp(PLSrun.SplitHalf, 'yes')
        ucorr_prob = PLSresult.perm_splithalf.ucorr_prob(LV);
        vcorr_prob = PLSresult.perm_splithalf.vcorr_prob(LV);
    end
    
    
    %===========================%
    % FOR MCENT & NROT RESULTS: %
    %===========================%   
    
    if ismember(PLSrun.Method, {'MCENT', 'NROT'})
        
        % Get design contrast:
        LVcontrasts = PLSresult.v(:,LV);                        % (Cond*Group)
        LVcontrasts = reshape(LVcontrasts, NumCond, NumGroup);  % (Cond,Group)
        LVcontrasts = permute(LVcontrasts, [2 1]);              % (Group,Cond)
        
        % Get brain-scores and calculate error:
        BrainScores = PLSresult.boot_result.orig_usc(:,LV);
        BrainScores = reshape(BrainScores, NumCond, NumGroup);
        BrainScores = permute(BrainScores, [2 1]);
        
        ULscores = PLSresult.boot_result.ulusc(:,LV);
        ULscores = reshape(ULscores, NumCond, NumGroup);
        ULscores = permute(ULscores, [2 1]);
        
        LLscores = PLSresult.boot_result.llusc(:,LV);
        LLscores = reshape(LLscores, NumCond, NumGroup);
        LLscores = permute(LLscores, [2 1]);
        
        UpperError = ULscores - BrainScores;
        LowerError = BrainScores - LLscores;
        
        % Each bar grouping represents a GroupID:
        BarGroups    = name.GroupID;
        NumBarGroups = length(BarGroups);
        if NumBarGroups ~= size(LVcontrasts, 1)
            disp('ERROR: Incorrect # of bar groups detected.')
			return;
        end
        
        
        %--- Plot design contrasts: ---%
        %------------------------------%
        figure(1);
        bar(LVcontrasts);
        hold on;

        % Set axis and tick labels:
        if NumBarGroups == 1
            set(gca, 'XTick',      [1:NumCond]);  % Have tick for each cond bar
            set(gca, 'XTickLabel', name.CondID);
            xlabel(name.GroupID, 'Interpreter', 'none');
        else
            set(gca, 'XTick',      [1:NumBarGroups]);  % Have tick for each bar-group
            set(gca, 'XTickLabel', BarGroups);
            xlabel('Groups:', 'Interpreter', 'none')
            LegendVar = legend(name.CondID);
            set(LegendVar, 'Interpreter', 'none');
        end
        ylabel('Contrast Coefficients:', 'Interpreter', 'none');
        
        % Set plot labels:
        if strcmp(PLSrun.SplitHalf, 'no')
            PlotTitle = sprintf('%s %s LV%d (p < %d)', PLSrun.Method, OutName, LV, pValue);
        else
            PlotTitle = sprintf('%s %s LV%d (p < %d, ucorr_prob < %d, vcorr_prob < %d)',...
                PLSrun.Method, OutName, LV, pValue, ucorr_prob, vcorr_prob);
        end
        title(PlotTitle, 'Interpreter', 'none');
        
        % Save plot:
        PlotFullpath = [OutFolder,'/',OutName,'_LV',num2str(LV),'cons.fig'];
		if exist(PlotFullpath, 'file')
			system(['rm ',PlotFullpath]);
		end
		
        saveas(gcf, PlotFullpath);
        hold off; close(1);
        
        
        %--- Plot brain scores & error-bars: ---%
        %---------------------------------------%
        figure(2);
        bar(BrainScores);
        hold on;
        
        % Plot error-bars for brain scores:
        GroupWidth = min(0.8, NumCond/(NumCond+1.5));  % Spacing between bar-groups
        for n = 1:NumCond  % For each cond bar
			if NumBarGroups == 1
				HorPos = n;
			else
				HorPos = (1:NumBarGroups) - GroupWidth/2 + (2*n-1) * GroupWidth / (2*NumCond);
			end
			
            errorbar(HorPos, BrainScores(:,n), LowerError(:,n), UpperError(:,n),...
                'k', 'linestyle', 'none');
        end
        
        % Set axis and tick labels:
        if NumBarGroups == 1
            set(gca, 'XTick', [1:NumCond]);  % Have tick for each cond bar
            set(gca, 'XTickLabel', name.CondID);
            xlabel(name.GroupID, 'Interpreter', 'none');
        else
            set(gca, 'XTick', [1:NumBarGroups]);  % Have tick for each bar-group
            set(gca, 'XTickLabel', BarGroups);
            xlabel('Groups:', 'Interpreter', 'none')
            LegendVar = legend(name.CondID);
            set(LegendVar, 'Interpreter', 'none');
        end
        ylabel('Mean-Centered LV Brain Scores (Orig. USC):', 'Interpreter', 'none');
        
        % Set plot labels:
        if strcmp(PLSrun.SplitHalf, 'no')
            PlotTitle = sprintf('%s %s LV%d (p < %d)', PLSrun.Method, OutName, LV, pValue);
        else
            PlotTitle = sprintf('%s %s LV%d (p < %d, ucorr_prob < %d, vcorr_prob < %d)',...
                PLSrun.Method, OutName, LV, pValue, ucorr_prob, vcorr_prob);
        end
        title(PlotTitle, 'Interpreter', 'none');
        
        % Save plot:
        PlotFullpath = [OutFolder,'/',OutName,'_LV',num2str(LV),'scores.fig'];
        if exist(PlotFullpath, 'file')
            system(['rm ',PlotFullpath]);
		end
		
        saveas(gcf, PlotFullpath);
        hold off; close(2);
         
    end
    
    
    
    %==============================================%
    % FOR BHV, MBLOCK, NRBHV, or NRMBLOCK RESULTS: %
    %==============================================%
    
    if ismember(PLSrun.Method, {'BHV', 'MBLOCK', 'NRBHV', 'NRMBLOCK'})
        
        % Get corr scores and calculate error:
        LVcorrs = PLSresult.boot_result.orig_corr(:,LV);        % (Bhv*Cond*Group)
        LVcorrs = reshape(LVcorrs, NumBhv, NumCond, NumGroup);  % (Bhv,Cond,Group)
        LVcorrs = permute(LVcorrs, [1 3 2]);                    % (Bhv,Group,Cond)
        LVcorrs = reshape(LVcorrs, [], NumCond);                % (Bhv*Group,Cond)
        
        ULcorrs = PLSresult.boot_result.ulcorr(:,LV);
        ULcorrs = reshape(ULcorrs, NumBhv, NumCond, NumGroup);
        ULcorrs = permute(ULcorrs, [1 3 2]);
        ULcorrs = reshape(ULcorrs, [], NumCond);
        
        LLcorrs = PLSresult.boot_result.llcorr(:,LV);
        LLcorrs = reshape(LLcorrs, NumBhv, NumCond, NumGroup);
        LLcorrs = permute(LLcorrs, [1 3 2]);
        LLcorrs = reshape(LLcorrs, [], NumCond);
        
        UpperError = ULcorrs - LVcorrs;
        LowerError = LVcorrs - LLcorrs;
        
         % Each bar group represents GroupID & Bhv:
        BarGroups = [];
        for g = 1:NumGroup
            for b = 1:NumBhv
                GroupBhvName = cellstr([name.GroupID{g},'_',name.BhvNames{b}]);
                BarGroups    = [BarGroups; GroupBhvName];  % Make label for each bar group
            end
		end
		
        NumBarGroups = length(BarGroups);
        if NumBarGroups ~= size(LVcorrs, 1)
			disp('ERROR: Incorrect # of bar groups detected.')
			return;
		end
        
        
        %--- Plot LV Correlations: ---%
        %-----------------------------%
        figure(1);
        bar(LVcorrs);
        hold on;
        
        % Plot error-bars:
        GroupWidth = min(0.8, NumCond/(NumCond+1.5));  % Spacing between bar-groups
        for n = 1:NumCond  % For each cond bar
            if NumBarGroups == 1
                HorPos = n;
            else
                HorPos = (1:NumBarGroups) - GroupWidth/2 + (2*n-1) * GroupWidth / (2*NumCond);
			end
			
            errorbar(HorPos, LVcorrs(:,n), LowerError(:,n), UpperError(:,n),...
                    'k', 'linestyle', 'none');
        end
        
        % Set plot labels:
        if NumBarGroups == 1
            set(gca, 'XTick', [1:NumCond]);  % Have tick for each cond bar
            set(gca, 'XTickLabel', name.CondID);
            xlabel(BarGroups{1}, 'Interpreter', 'none');
        else
            set(gca, 'XTick', [1:NumBarGroups]);  % Have tick for each bar group
            set(gca, 'XTickLabel', BarGroups);
            xlabel('Groups & Behaviors:', 'Interpreter', 'none')
            LegendVar = legend(name.CondID);
            set(LegendVar, 'Interpreter', 'none');
        end
        ylabel('Correlation Coefficients:', 'Interpreter', 'none')
        
        % Set plot title:
        if strcmp(PLSrun.SplitHalf, 'no')
            PlotTitle = sprintf('%s %s LV%d (p < %d)', PLSrun.Method, OutName, LV, pValue);
        else
            PlotTitle = sprintf('%s %s LV%d (p < %d, ucorr_prob < %d, vcorr_prob < %d)',...
                PLSrun.Method, OutName, LV, pValue, ucorr_prob, vcorr_prob);
        end
        title(PlotTitle, 'Interpreter', 'none');
        
        % Save plot:
        PlotFullpath = [OutFolder,'/',OutName,'_LV',num2str(LV),'.fig'];
		
        CheckSavePath(PlotFullpath, 'RunPLS');
        
        saveas(gcf, PlotFullpath);
        hold off; close(1);
        
    end  % RunMethod
end  % LV

fclose(PlotErrLog);
