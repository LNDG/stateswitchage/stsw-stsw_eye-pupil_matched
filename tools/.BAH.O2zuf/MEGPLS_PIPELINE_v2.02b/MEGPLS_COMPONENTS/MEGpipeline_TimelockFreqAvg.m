%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Group averages timelock or frequency analysis files. %
% Last modified: Feb. 7, 2014                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_TimelockFreqAvg(BuilderMat, AnalysisType)


% Make sure java paths added:
% Note: Using parfor progress bar even though no parfor.
% - Other components already using it, and check needs to be run anyway.
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
FTcfg        = Builder.FTcfg;


% Clear existing Errorlog & Diary:
switch AnalysisType
    case 'Timelock'
        RunSection = 'TimelockAvg';
    case 'Freq'
        RunSection = 'FreqAvg';
end

if exist(['ErrorLog_',RunSection,'.txt'], 'file')
    system(['rm ErrorLog_',RunSection,'.txt']);
end
if exist(['Diary_',RunSection,'.txt'], 'file')
    system(['rm Diary_',RunSection,'.txt']);
end

diary(['Diary_',RunSection,'.txt']);
ErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');



%=======================================%
% GROUP-AVERAGE TIMELOCK OR FREQ FILES: %
%=======================================%

for g = 1:length(name.GroupID)
    NumCond = length(name.CondID);
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['GROUP-AVERAGING TIME/FREQ DATA:  ',name.GroupID{g},'.  '], NumCond, 1, 700, 80);
    end
    
    % Note: Do NOT parfor here, want proper ordering.
    for c = 1:length(name.CondID)
        
        TargetFiles  = [];  % Reset input
        MissingFiles = 0;
                
        % Compile & check subject files to avg:
        for s = 1:length(name.SubjID{g})
            switch AnalysisType
                case 'Timelock'
                    TargetFiles{s} = paths.Timelock{g}{s,c};
                case 'Freq'
                    TargetFiles{s} = paths.Freq{g}{s,c};
            end
            
            CheckInput = CheckPipelineMat(TargetFiles{s}, RunSection);
            if CheckInput == 0
                MissingFiles = 1;
                continue;
            end
        end  % Subj
        
        % Skip condition if errors found:
        if MissingFiles == 1
            if strcmp(AnalysisType, 'Timelock')
                fprintf(ErrLog, ['ERROR: Failed to group-avg data (missing subject data):'...
                    '\n %s \n\n'], paths.TimelockGrpAvg{g}{c});
                
            elseif strcmp(AnalysisType, 'Freq')
                fprintf(ErrLog, ['ERROR: Failed to group-avg data (missing subject data):'...
                    '\n %s \n\n'], paths.FreqGrpAvg{g}{c});
            end
            
            disp('ERROR: Missing .mat file(s) for GPAVG. Skipping this condition.')
            continue;
        end
        
        % Average files:
        switch AnalysisType
            case 'Timelock'
                CheckSavePerms(paths.TimelockGrpAvg{g}{c}, RunSection);
                
                cfgTimelockAvg                   = [];
                cfgTimelockAvg                   = FTcfg.TimelockAvg;
                cfgTimelockAvg.inputfile         = TargetFiles;
                cfgTimelockAvg.outputfile        = paths.TimelockGrpAvg{g}{c};
                cfgTimelockAvg.outputfilepresent = 'overwrite';
                
                ft_timelockgrandaverage(cfgTimelockAvg)
                
                
                % Compute global mean field power for grpavg timelock data:
                if exist(paths.TimelockGrpAvg{g}{c}, 'file')
                    CheckSavePerms(paths.GlobalMeanFieldGrpAvg{g}{c}, RunSection);
                    
                    cfgGFP                   = [];
                    cfgGFP                   = FTcfg.GFP;
                    cfgGFP.inputfile         = paths.TimelockGrpAvg{g}{c};
                    cfgGFP.outputfile        = paths.GlobalMeanFieldGrpAvg{g}{c};
                    cfgGFP.outputfilepresent = 'overwrite';
                    
                    ft_globalmeanfield(cfgGFP)  % Compute GMFP from new timelockgrpavg file.
                end
                
                
            case 'Freq'
                CheckSavePerms(paths.FreqGrpAvg{g}{c}, RunSection);
                
                cfgFreqAvg                   = [];
                cfgFreqAvg                   = FTcfg.FreqAvg;
                cfgFreqAvg.inputfile         = TargetFiles;
                cfgFreqAvg.outputfile        = paths.FreqGrpAvg{g}{c};
                cfgFreqAvg.outputfilepresent = 'overwrite';
                
                ft_freqgrandaverage(cfgFreqAvg)
                
        end
        
        if UseProgBar == 1 && mod(c, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Cond
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
    for c = 1:length(name.CondID)
        
        switch AnalysisType
            case 'Timelock'
                if ~exist(paths.TimelockGrpAvg{g}{c}, 'file')
                    fprintf(ErrLog, ['ERROR: Output timelock group-avg file missing:'...
                        '\n %s \n\n'], paths.TimelockGrpAvg{g}{c});
                end
                if ~exist(paths.GlobalMeanFieldGrpAvg{g}{c}, 'file')
                    fprintf(ErrLog, ['ERROR: Output global mean field group-avg file missing:'...
                        '\n %s \n\n'], paths.GlobalMeanFieldGrpAvg{g}{c});
                end
                
            case 'Freq'
                if ~exist(paths.FreqGrpAvg{g}{c}, 'file')
                    fprintf(ErrLog, ['ERROR: Output timefreq group-avg file missing:'...
                        '\n %s \n\n'], paths.FreqGrpAvg{g}{c});
                end
        end
        
    end  % Cond
end  % Group



%=================%

if exist([pwd,'/ErrorLog_',RunSection,'.txt'], 'file')
    LogCheck = dir(['ErrorLog_',RunSection,'.txt']);
    if LogCheck.bytes ~= 0  % File not empty
        open(['ErrorLog_',RunSection,'.txt']);
    else
        delete(['ErrorLog_',RunSection,'.txt']);
    end
end

fclose(ErrLog);
diary off
