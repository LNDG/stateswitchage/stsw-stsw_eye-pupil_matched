%% Create group structure

% 1223 is missing runs 2-4
% 1228 is missing 58 trials (run 4)

%% paths

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data_in    = fullfile(rootpath, 'data', 'A_pupilDiameter_cutByStim');
pn.data_out    = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools));

%% load data & create proper data structures with summary measures

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

% input data are 1000 Hz
inputFs = 1000;
targetFs = 100;

pupilDataFT = cell(1,4);
for indID = 1:numel(IDs)
    disp(['Processing ', IDs{indID}])
    load(fullfile(pn.data_in,[IDs{indID}, '_pupilData.mat']), ...
        'pupilData', 'TrlInfo');
    for indCond = 1:4
        N_trials_available = size(pupilData.dataStim,1);
        disp(['Trials: ', num2str(N_trials_available)])
        condTrials = TrlInfo.StateOrders(1:N_trials_available)==indCond;
        tmpData = squeeze(pupilData.dataStim(condTrials,:));
        % downsample to <targetFs> Hz
        pupilDataFT{indCond}.data(indID,1:numel(find(condTrials)),:) = ...
            ft_preproc_resample(tmpData, inputFs, targetFs, 'resample');
        % create first derivative
        derivData = diff(pupilDataFT{indCond}.data(indID,:,:),1,3);
%         Bltime = find(pupilDataFT{indCond}.time > -.5 & pupilDataFT{indCond}.time < 0);
%         meanBL = nanmean(derivData(:,:,Bltime+1),3);
%         meanBL = repmat(meanBL, 1,1,Ntime); % trial-wise baseline
        pupilDataFT{indCond}.dataDeriv(indID,:,:) = derivData;%(pupilDataFT{indCond}.data-meanBL)./meanBL;
        % smooth the first derivative
        tmpSmooth = smoothdata(derivData,3,'movmedian', 30); % 100 Hz input data, 300 ms smoothing
        pupilDataFT{indCond}.dataDerivSmooth(indID,:,:) = tmpSmooth;
        % add some info
        pupilDataFT{indCond}.dimord = 'subj_rpt_time';
        pupilDataFT{indCond}.time = ft_preproc_resample(pupilData.time, inputFs, targetFs, 'resample');
        pupilDataFT{indCond}.Fs = targetFs;
        pupilDataFT{indCond}.descriptor = ['Pupil data for load ', num2str(indCond)];
    end
end

%% create summary measures

summaryPupil = []; tmp_stimTime = find(pupilDataFT{1}.time>3 & pupilDataFT{1}.time<4.5);
for indCond = 1:4
    summaryPupil.stimPupilDeriv(:,indCond) = ...
        squeeze(nanmean(nanmedian(pupilDataFT{indCond}.dataDeriv(:,:,tmp_stimTime),3),2));
    summaryPupil.IDs = IDs;
end

% calculate linear change in all measures

X = [1,1; 1,2; 1,3; 1,4];
b=X\summaryPupil.stimPupilDeriv'; % matrix implementation of multiple regression        
summaryPupil.stimPupilDeriv_slopes = b(2,:);

%% save results

save(fullfile(pn.data_out, 'G1_summaryPupil.mat'), 'summaryPupil', 'IDs')
save(fullfile(pn.data_out, 'G1_pupilDataFT.mat'), 'pupilDataFT')
