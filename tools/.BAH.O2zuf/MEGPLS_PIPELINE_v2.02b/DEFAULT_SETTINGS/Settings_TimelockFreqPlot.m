%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVANCED SETTINGS FOR PLOTTING TIMELOCK / FREQ RESULTS: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This settings file will be read by the VisualizeTimeFreq GUI.
% - Settings here will be applied to timelock or frequency analysis plots.
%
% Usage:
%  a)  Settings under "DO NOT EDIT" are handled by [MEG]PLS. Do not alter these.
%  b)  Change other values as needed. See "help" command for FieldTrip section.
%  c)  Optional settings can be uncommented and specified.
%
function [cfgPlotER, cfgPlotTFR] = Settings_TimelockFreqPlot(LineStyles)



%--- "FT_MULTIPLOTER" & "FT_SINGLEPLOTER" SETTINGS: ---%
%------------------------------------------------------%

% DO NOT EDIT:
cfgPlotER.channel     = 'MEG';
cfgPlotER.linestyle   = LineStyles;
cfgPlotER.interactive = [];  % Handled by GUI.


% BASELINE SETTINGS:
cfgPlotER.baseline      = 'no';         % Specify baseline as: [TimeStart, TimeEnd]
cfgPlotER.baselinetype  = 'absolute';   % 'absolute' or 'relative'


% GENERAL SETTINGS:
cfgPlotER.layout      = 'CTF151.lay';
cfgPlotER.axes        = 'yes';
cfgPlotER.box         = 'no';
cfgPlotER.showlabels  = 'yes';
cfgPlotER.showoutline = 'no';
cfgPlotER.hotkeys     = 'yes';
cfgPlotER.maskstyle   = 'saturation';
cfgPlotER.trials      = 'all';


% OPTIONAL SETTINGS:
%  cfgPlotER.parameter     = field to be plotted on y-axis (default depends on data.dimord)
%  cfgPlotER.maskparameter = field in the first dataset to be used for marking significant data

%  cfgPlotER.xlim          = 'maxmin' or [xmin xmax] (default = 'maxmin')
%  cfgPlotER.ylim          = 'maxmin', 'maxabs', or [ymin ymax] (default = 'maxmin')
%  cfgPlotER.refchannel    = name of reference channel for visualising connectivity, can be 'gui'

%  cfgPlotER.comment        = string of text (default = date + colors)
%  cfgPlotER.fontsize       = font size of comment and labels (if present) (default = 8)
%  cfgPlotER.renderer       = 'painters', 'zbuffer',' opengl' or 'none' (default = [])
%  cfgPlotER.linewidth      = linewidth in points (default = 0.5)
%  cfgPlotER.graphcolor     = color(s) used for plotting the dataset(s)
%  cfgPlotER.directionality = '', 'inflow' or 'outflow



%--- "FT_MULTIPLOTTFR" & "FT_SINGLEPLOTTFR" SETTINGS: ---%
%--------------------------------------------------------%

% DO NOT EDIT:
cfgPlotTFR.channel     = 'MEG';
cfgPlotER.linestyle    = LineStyles;
cfgPlotTFR.interactive = [];  % Handled by GUI.


% BASELINE SETTINGS:
cfgPlotTFR.baseline      = 'no';         % Specify baseline as: [TimeStart, TimeEnd]
cfgPlotTFR.baselinetype  = 'absolute';   % 'absolute' or 'relative'


% GENERAL SETTINGS:
cfgPlotTFR.layout      = 'CTF151.lay';
cfgPlotTFR.box         = 'yes';
cfgPlotTFR.showlabels  = 'yes';
cfgPlotTFR.showoutline = 'no';
cfgPlotTFR.hotkeys     = 'yes';
cfgPlotTFR.maskstyle   = 'saturation';
cfgPlotTFR.trials      = 'all';

cfgPlotTFR.xlim        = 'maxmin';  % 'maxmin' or [xmin xmax] (default = 'maxmin')
cfgPlotTFR.ylim        = 'maxmin';  % 'maxmin' or [ymin ymax] (default = 'maxmin')
cfgPlotTFR.zlim        = 'maxmin';  % 'maxmin','maxabs' or [zmin zmax] (default = 'maxmin')


% OPTIONAL SETTINGS:
%  cfgPlotTFR.parameter      = field to be plotted on y-axis (default depends on data.dimord)
%  cfgPlotTFR.maskparameter  = field in the first dataset to be used for marking significant data
%  cfgPlotTFR.maskalpha      = alpha value between 0 (transparant) and 1 (opaque) used for masking areas dictated by cfgPlotTFR.maskparameter (default = 1)
%  cfgPlotTFR.masknans       = 'yes' or 'no' (default = 'yes')
%  cfgPlotTFR.refchannel     = name of reference channel for visualising connectivity, can be 'gui'

%  cfgPlotTFR.gradscale      = number, scaling to apply to the MEG gradiometer channels prior to display
%  cfgPlotTFR.magscale       = number, scaling to apply to the MEG magnetometer channels prior to display
%  cfgPlotTFR.colorbar       = 'yes', 'no' (default = 'no')
%  cfgPlotTFR.colormap       = any sized colormap, see COLORMAP

%  cfgPlotTFR.comment        = string of text (default = date + zlimits)
%  cfgPlotTFR.fontsize       = font size of comment and labels (if present) (default = 8)
%  cfgPlotTFR.renderer       = 'painters', 'zbuffer',' opengl' or 'none' (default = [])
%  cfgPlotTFR.directionality = '', 'inflow' or 'outflow'
