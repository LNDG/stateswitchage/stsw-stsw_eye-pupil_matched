[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Pupil dilation

Pupil diameter was recorded during the EEG session using EyeLink 1000 at a sampling rate of 1000 Hz and was analyzed using FieldTrip and custom-written MATLAB scripts. Blinks were automatically indicated by the EyeLink software (version 4.40). To increase the sensitivity to periods of partially occluded pupils or eye movements, the first derivative of eye-tracker-based vertical eye movements was calculated, z-standardized, and outliers >= 3 STD were removed. We additionally removed data within 150 ms preceding or following indicated outliers. Finally, missing data were linearly interpolated, and data were epoched to 3.5 s prior to stimulus onset to 1 s following stimulus offset. We quantified phasic arousal responses via the 1st temporal derivative (i.e. rate of change) of pupil diameter traces as this measure (i) has higher temporal precision and (ii) has been more strongly associated with noradrenergic responses than the overall response 26. We downsampled pupil time series to 200 Hz. For visualization, but not statistics, we smoothed pupil traces using a moving average median of 200 ms. We statistically assessed a linear load effect using a cluster-based permutation test on the 1D pupil traces (see Univariate statistical analyses using cluster-based permutation tests). For post-hoc assessments, we extracted the median pupil derivative during the first 1.5 s following stimulus onset.

## Function overview

**a_pupil_preproc**

- Pupil preprocessing
- To increase the sensitivity to periods of partially occluded pupils or eye movements, the first derivative of eye-tracker-based vertical eye movements was calculated, z-standardized, and outliers >= 3 STD were removed. We additionally removed data within 150 ms preceding or following indicated artifacts.
- missing data were linearly interpolated
- data were epoched to 3.5 s prior to stimulus onset to 1 s following stimulus offset

**b_create_group_structure**

- collect individual data in output structure
- downsample to 100 Hz (this was 200 Hz in Kosciessa et al., 2021)
- calculate 1st temporal derivative (i.e. rate of change) of pupil diameter
- apply movmedian smoothing for display (30 samples -> 300ms moving windows) (this was 200ms in Kosciessa et al., 2021)

**c_phasic_1stDeriv_stats**

- CBPA for linear changes

**d_phasic_1stDeriv_traces**

- plot pupil traces with statistics overlay

YA:
![figure](figures/d_PupilDerivative_StimOnset_ya.png)

OA:
![figure](figures/d_PupilDerivative_StimOnset_oa.png)

**e01_taskPLS_load_YAOA**

- task PLS of first derivative of pupil

**e02_taskPLS_load_YA_OA_plotLV1**

Brainscore Trace:
![figure](figures/e01_pls_traces.png)

RCP:
![figure](figures/e01_pls_rcp.png)