clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

d_phasic_1stDeriv_traces('ya')
d_phasic_1stDeriv_traces('oa')
d_phasic_1stDeriv_traces('yaoa')

disp('Step finished!')

close all; clc;