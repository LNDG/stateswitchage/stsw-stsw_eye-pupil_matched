%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates 3D and 4D NIFTI files for NormSource files. %
% Note: NormSource files are already interpolated.    %
% Last modified: Jan. 15, 2014                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_MakeNormSourceNii_Wrap(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
MainFTcfg    = Builder.FTcfg;
PipeSettings = Builder.PipeSettings;


% Clear existing Errorlog & Diary:
if exist('ErrorLog_MakeNormSourceNii.txt', 'file')
    system('rm ErrorLog_MakeNormSourceNii.txt');
end
if exist('Diary_MakeNormSourceNii.txt', 'file')
    system('rm Diary_MakeNormSourceNii.txt');
end

diary Diary_MakeNormSourceNii.txt
ErrLog = fopen('ErrorLog_MakeNormSourceNii.txt', 'a');



%=======================================%
% GENERATE NIFTI FILES FOR NORM SOURCE: %
%=======================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['GENERATING NORM-SOURCE NIFTI FILES:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
    for s = 1:length(name.SubjID{g})
        for c = 1:length(name.CondID)
            
            
            %--- Create 3D NIFTI files: ---%
            %------------------------------%
            
            parfor t = 1:size(time.Windows, 1)
                CheckSavePath(paths.NormSourceNii{g}{s,c,t}, 'MakeNormSourceNii');
                
                cfgVolumeWrite = [];
                cfgVolumeWrite = MainFTcfg.WriteNormSrc;
                
                MEGpipeline_MakeNifti3D_Guts...
                    (cfgVolumeWrite, paths.NormSource{g}{s,c,t}, paths.NormSourceNii{g}{s,c,t})
                
            end  % Time
            
            
            %--- Merge 3D NIFTI files into 4D NIFTI: ---%
            %-------------------------------------------%
            
            % Make list of files to merge:
            MergeFileList = [];
            MissingFiles  = 0;
            
            for t = 1:size(time.Windows, 1)  % Do NOT want to parfor here, keep proper order
                MergeFileList{t}  = paths.NormSourceNii{g}{s,c,t};  %** Is the ",1" needed?
                
                if ~exist(paths.NormSourceNii{g}{s,c,t}, 'file')
                    [Folder, File, ~] = fileparts(paths.NormSourceNii{g}{s,c,t});
                    
                    if MissingFiles == 0
                        MissingFiles = 1;
                        fprintf(ErrLog, ['\nERROR: NormSource 3D .nii file(s) missing:'...
                            '\n %s \n'], Folder);
                    end
                    
                    fprintf(ErrLog, ' - Time: %s \n', [File,'.nii']);
                end
            end
            
            if MissingFiles == 1
                disp('ERROR: Missing 3D .nii file(s). Skipping dataset.')
                continue;
            end
            
            % Merge 3D NIFTI files into 4D NIFTI:
            CheckSavePath(paths.Nifti4DNormSource{g}{s,c}, 'MakeNormSourceNii');
            MEGpipeline_MakeNifti4D_Guts(MergeFileList, paths.Nifti4DNormSource{g}{s,c})
            
            % Remove intermediate 3D NIFTI files:
            if strcmp(PipeSettings.Afni4D.KeepNifti3D, 'no')
                for f = 1:length(MergeFileList)
                    system(['rm ',MergeFileList{f}]);
                end
            end
            
            
        end  % Cond
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group

if strcmp(PipeSettings.Afni4D.KeepNifti3D, 'no')
    if exist([paths.AnalysisID,'/NORM_SOURCE_Nifti3D'], 'dir')
        system(['rm -R ',paths.AnalysisID,'/NORM_SOURCE_Nifti3D']);
    else
        disp('WARNING: Could not find NORM_SOURCE_Nifti3D folder for removal.');
    end
end



%==========================================================%
% GENERATES TIME-LEGEND FOR INDICES IN NIFTI & AFNI FILES: %
%==========================================================%

if exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
	system(['rm ',paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end
TimeLegend = fopen('NiftiAfni4D_TimeLegend.txt', 'a');

fprintf(TimeLegend, ['/// IMPORTANT: ///'...
	'\n - In the AFNI-VIEWER, voxel and time indices start at 0.    '...
    '\n   In the data however, voxel and time indices start at 1. \n'...
    '\n - Therefore, when selecting indices for use outside of the  '...
    '\n   AFNI-viewer or AFNI functions, make sure you are using the'...
    '\n   data index, and NOT the AFNI-viewer index. \n\n']);

fprintf(TimeLegend, ['/// TIME LEGEND: ///'...
	'\n AFNI-Viewer ------- AFNI/NIFTI ------- TIME-Interval:'...
	'\n ViewerIndex ------- Data Index ------- In seconds:']);

for t = 1:size(time.Windows, 1)
	fprintf(TimeLegend, '\n---- %s --------------- %s ------------ %s',...
		num2str(t-1), num2str(t), time.str.Windows{t,3});
end

fclose(TimeLegend);
if ~isequal(pwd, paths.AnalysisID)
    movefile('NiftiAfni4D_TimeLegend.txt', [paths.AnalysisID,'/'], 'f');
end



%==========================%
% CHECKS FOR OUTPUT FILES: %
%==========================%

for g = 1:length(name.GroupID)
	for s = 1:length(name.SubjID{g})
		for c = 1:length(name.CondID)
			
			if ~exist(paths.Nifti4DNormSource{g}{s,c}, 'file')
				fprintf(ErrLog, ['ERROR: Output 4D-NIFTI file missing:'...
					'\n %s \n\n'], paths.Nifti4DNormSource{g}{s,c});
			end

		end  % Cond
	end  % Subj
end  % Group

if ~exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
	fprintf(ErrLog, ['ERROR: Time-legend file for NIFTI/AFNI files missing:'...
		'\n %s \n\n'], [paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end



%=================%

if exist([pwd,'/ErrorLog_MakeNormSourceNii.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNormSourceNii.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNormSourceNii.txt');
    else
        delete('ErrorLog_MakeNormSourceNii.txt');
    end
end

if exist([pwd,'/ErrorLog_MakeNifti3D.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNifti3D.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNifti3D.txt');
    else
        delete('ErrorLog_MakeNifti3D.txt');
    end
end

if exist([pwd,'/ErrorLog_MakeNifti4D.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNifti4D.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNifti4D.txt');
    else
        delete('ErrorLog_MakeNifti4D.txt');
    end
end

fclose(ErrLog);
diary off
