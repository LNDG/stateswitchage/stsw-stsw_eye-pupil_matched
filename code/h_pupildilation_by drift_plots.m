pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/';
pn.fieldtrip        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/fieldtrip-20170904/']; addpath(pn.fieldtrip); ft_defaults;
pn.shadedError      = [pn.root, 'B_analyses/A_pupilDilation/T_tools/shadedErrorBar/']; addpath(pn.shadedError);
pn.naninterp        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/naninterp/']; addpath(pn.naninterp);
pn.behavior         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
pn.eyeDataEEG       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/C_EEG_FT/';
pn.eyeDataMrk       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/B_EEG_ET_ByRun/';
pn.collectedDataOut = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];
pn.statsOut         = [pn.root, 'B_analyses/A_pupilDilation/B_data/C_stats/'];
pn.dataOut          = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];

%% load results

load([pn.dataOut, 'G2_summaryPupil_raw.mat'], 'summaryPupil')
load([pn.dataOut, 'G2_pupilDataFT_raw.mat'], 'pupilDataFT')

IDs = summaryPupil.IDs;

idxYA = cell2mat(cellfun(@str2num, IDs, 'un', 0))<2000;
idxOA = cell2mat(cellfun(@str2num, IDs, 'un', 0))>2000;
ageIndices{1} = idxYA; ageName{1} = 'YoungAdults';
ageIndices{2} = idxOA; ageName{2} = 'OldAdults';

%% load results

load([pn.statsOut, 'G2_parametric_raw.mat'], 'cfgStat', 'stat*')

%% plot temporal traces with statistics

catAll = cat(4,pupilDataFT{1}.data, pupilDataFT{2}.data, pupilDataFT{3}.data,...
        pupilDataFT{4}.data);

time = pupilDataFT{1}.time-3;

h = figure('units','normalized','position',[.1 .1 .25 .6]);
for indAge = 1:2
    subplot(2,1,indAge);
    cla; hold on; 
    % highlight different experimental phases in background
    patches.timeVec = [3 6]-3;
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [7000 10000];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    condAvg = squeeze(nanmean(nanmean(catAll(ageIndices{indAge},:,:,:),2),4));

    curData = squeeze(nanmean(pupilDataFT{1}.data(ageIndices{indAge},:,:),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(ageIndices{indAge})),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

    curData = squeeze(nanmean(pupilDataFT{2}.data(ageIndices{indAge},:,:),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(ageIndices{indAge})),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

    curData = squeeze(nanmean(pupilDataFT{3}.data(ageIndices{indAge},:,:),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(ageIndices{indAge})),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

    curData = squeeze(nanmean(pupilDataFT{4}.data(ageIndices{indAge},:,:),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(ageIndices{indAge})),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'L1'; 'L2'; 'L3'; 'L4'}, 'orientation', 'horizontal', 'location', 'North'); legend('boxoff');

    xlim([1.75 6.25]-3);  
    if indAge == 1 
        ylim([7500 9500]);
    else ylim([6000 7000]);
    end
    ylabel('Pupil diameter (raw)'); xlabel('Time (s from stim onset)')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
end

%% median split based on L1 drift rates


pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary_YAOA.mat'], 'STSWD_summary')

h = figure('units','normalized','position',[.1 .1 .4 .2]); 
for indAge = 1:2
    subplot(1,2,indAge); cla; hold on;

    idxsummary = ismember(STSWD_summary.IDs, IDs(ageIndices{indAge}));
    drift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idxsummary,1),2));
    [driftIdx, sortIdx] = sort(drift, 'descend');
    highdrift = sortIdx(1:ceil(numel(sortIdx)/2));
    lowdrift = sortIdx(ceil(numel(sortIdx)/2)+1:numel(sortIdx));
    curageIDs = find(ageIndices{indAge});

    idx_ids = curageIDs(lowdrift);
%     condAvg = squeeze(nanmean(nanmean(catAll(idx_ids,:,:,:),2),4));
    curData = squeeze(nanmean(pupilDataFT{1}.data(idx_ids,:,:),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(idx_ids),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

    idx_ids = curageIDs(highdrift);
%     condAvg = squeeze(nanmean(nanmean(catAll(idx_ids,:,:,:),2),4));
    curData = squeeze(nanmean(pupilDataFT{1}.data(idx_ids,:,:),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(idx_ids),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

    legend([l1.mainLine, l4.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')
    
    xlim([-3 3.75]);  
    if indAge == 1 
        ylim([7000 9500]);
    else ylim([5500 7500]);
    end
end